package user

import (
	"context"

	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/tools/cryptography"
	"gitlab.com/jakinss321/microservice_user/pkg/models"
	"gitlab.com/jakinss321/microservice_user/pkg/modules/user/service"
)

type serverGRPC struct {
	userService  service.Userer
	tokenService cryptography.TokenManager
}

func NewServiceGRPC(userService service.Userer, tokenService cryptography.TokenManager) *serverGRPC {
	return &serverGRPC{
		userService:  userService,
		tokenService: tokenService,
	}
}

func (s *serverGRPC) CreateUser(ctx context.Context, in *UserCreateIn) (*UserCreateOut, error) {
	userCreateIn := &service.UserCreateIn{
		Name:     in.Name,
		Phone:    in.Phone,
		Email:    in.Email,
		Password: in.Password,
		Role:     int(in.Role),
	}

	out := s.userService.Create(ctx, *userCreateIn)

	outProto := &UserCreateOut{
		UserId:    int32(out.UserID),
		ErrorCode: int32(out.ErrorCode),
	}
	return outProto, nil
}

func (s *serverGRPC) ChangePassword(ctx context.Context, in *ChangePasswordRequest) (*ChangePasswordResponse, error) {
	inChangePass := &service.ChangePasswordIn{
		UserID:      int(in.UserId),
		OldPassword: in.OldPassword,
		NewPassword: in.NewPassword,
	}

	out := s.userService.ChangePassword(ctx, *inChangePass)

	outProto := &ChangePasswordResponse{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}
	return outProto, nil
}

func (s *serverGRPC) GetUserByEmail(ctx context.Context, in *GetUserByEmailRequest) (*UserOut, error) {
	inEmail := &service.GetByEmailIn{
		Email: in.Email,
	}

	out := s.userService.GetByEmail(ctx, *inEmail)

	usr := &User{
		Id: int32(out.User.ID),
		// Name:          out.User.Name,
		// Phone:         out.User.Phone,
		Email:    out.User.Email,
		Password: out.User.Password,
		// Role:          int32(out.User.Role),
		// Verified:      out.User.Verified,
		// EmailVerified: out.User.EmailVerified,
		// PhoneVerified: out.User.PhoneVerified,
	}

	outProto := &UserOut{
		User:      usr,
		ErrorCode: int32(out.ErrorCode),
	}

	return outProto, nil
}

func (s *serverGRPC) GetUserByPhone(ctx context.Context, in *GetUserByPhoneRequest) (*UserOut, error) {
	inPhone := &service.GetByPhoneIn{
		Phone: in.Phone,
	}

	out := s.userService.GetByPhone(ctx, *inPhone)

	usr := &User{
		Id:            int32(out.User.ID),
		Name:          out.User.Name,
		Phone:         out.User.Phone,
		Email:         out.User.Email,
		Password:      out.User.Password,
		Role:          int32(out.User.Role),
		Verified:      out.User.Verified,
		EmailVerified: out.User.EmailVerified,
		PhoneVerified: out.User.PhoneVerified,
	}

	outProto := &UserOut{
		User:      usr,
		ErrorCode: int32(out.ErrorCode),
	}
	return outProto, nil
}

func (s *serverGRPC) GetUserByID(ctx context.Context, in *GetByIDRequest) (*UserOut, error) {
	inID := &service.GetByIDIn{
		UserID: int(in.UserId),
	}

	out := s.userService.GetByID(ctx, *inID)

	usr := &User{
		Id:            int32(out.User.ID),
		Name:          out.User.Name,
		Phone:         out.User.Phone,
		Email:         out.User.Email,
		Password:      out.User.Password,
		Role:          int32(out.User.Role),
		Verified:      out.User.Verified,
		EmailVerified: out.User.EmailVerified,
		PhoneVerified: out.User.PhoneVerified,
	}

	outProto := &UserOut{
		User:      usr,
		ErrorCode: int32(out.ErrorCode),
	}
	return outProto, nil
}

func (s *serverGRPC) GetUsersByID(ctx context.Context, in *GetByIDsRequest) (*UsersOut, error) {
	userIDs := &service.GetByIDsIn{
		UserIDs: make([]int, len(in.UserIds)),
	}
	for i, id := range in.UserIds {
		userIDs.UserIDs[i] = int(id)
	}

	out := s.userService.GetByIDs(ctx, *userIDs)

	var users []*User
	for _, user := range out.User {
		u := &User{
			Id:            int32(user.ID),
			Name:          user.Name,
			Phone:         user.Phone,
			Email:         user.Email,
			Password:      user.Password,
			Role:          int32(user.Role),
			Verified:      user.Verified,
			EmailVerified: user.EmailVerified,
			PhoneVerified: user.PhoneVerified,
		}
		users = append(users, u)
	}

	outProto := &UsersOut{
		User:      users,
		ErrorCode: int32(out.ErrorCode),
	}

	return outProto, nil
}

func (s *serverGRPC) UpdateUser(ctx context.Context, in *UserUpdateIn) (*UserUpdateOut, error) {
	userUpdateIn := &service.UserUpdateIn{
		User: models.User{
			ID:            int(in.User.Id),
			Name:          in.User.Name,
			Phone:         in.User.Phone,
			Email:         in.User.Email,
			Password:      in.User.Password,
			Role:          int(in.User.Role),
			Verified:      in.User.Verified,
			EmailVerified: in.User.EmailVerified,
			PhoneVerified: in.User.PhoneVerified,
		},
		Fields: int32SliceToIntSlice(in.Fields),
	}

	out := s.userService.Update(ctx, *userUpdateIn)

	outProto := &UserUpdateOut{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}
	return outProto, nil
}

func (s *serverGRPC) VerifyEmail(ctx context.Context, in *UserVerifyEmailRequest) (*UserUpdateResponse, error) {
	inVerify := &service.UserVerifyEmailIn{
		UserID: int(in.UserId),
	}

	out := s.userService.VerifyEmail(ctx, *inVerify)

	outProto := &UserUpdateResponse{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}
	return outProto, nil
}

func (s *serverGRPC) ParseToken(ctx context.Context, in *TokenRequest) (*UserClaimsResponse, error) {

	out, err := s.tokenService.ParseToken(in.Token, 0)
	if err != nil {
		return nil, err
	}

	return &UserClaimsResponse{
		Id:     out.ID,
		Role:   out.Groups,
		Layers: out.Layers,
	}, nil
}

func (s *serverGRPC) mustEmbedUnimplementedUserServiceServer() {
	panic("")
}
func int32SliceToIntSlice(a []int32) []int {
	b := make([]int, len(a))
	for i, v := range a {
		b[i] = int(v)
	}
	return b
}
