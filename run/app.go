package run

import (
	"context"
	"fmt"
	"net/http"
	"net/rpc"
	"os"

	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab.com/jakinss321/microservice_user/config"
	"gitlab.com/jakinss321/microservice_user/pkg/db"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/cache"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/component"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/db/migrate"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/db/scanner"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/errors"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/responder"
	"gitlab.com/jakinss321/microservice_user/rpc/user"
	"google.golang.org/grpc"

	usr "gitlab.com/jakinss321/microservice_user/grpc/user"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/router"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/server"
	internal "gitlab.com/jakinss321/microservice_user/pkg/infrastructure/service"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/tools/cryptography"
	"gitlab.com/jakinss321/microservice_user/pkg/models"
	"gitlab.com/jakinss321/microservice_user/pkg/modules"
	"gitlab.com/jakinss321/microservice_user/pkg/provider"
	"gitlab.com/jakinss321/microservice_user/pkg/storages"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf     config.AppConf
	logger   *zap.Logger
	srv      server.Server
	jsonRPC  server.Server
	Sig      chan os.Signal
	Storages *storages.Storages
	Servises *modules.Services
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	// запускаем json rpc сервер
	// errGroup.Go(func() error {
	// 	err := a.jsonRPC.Serve(ctx)
	// 	if err != nil {
	// 		a.logger.Error("app: server error", zap.Error(err))
	// 		return err
	// 	}
	// 	return nil
	// })

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// инициализация емейл провайдера
	email := provider.NewEmail(a.conf.Provider.Email, a.logger)
	// инициализация сервиса нотификации
	notifyEmail := internal.NewNotify(a.conf.Provider.Email, email, a.logger)
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, notifyEmail, tokenManager, responseManager, decoder, hash, a.logger)
	// инициализация сканера таблиц
	tableScanner := scanner.NewTableScanner()
	// регистрация таблиц
	tableScanner.RegisterTable(
		&models.UserDTO{},
		&models.EmailVerifyDTO{},
		&models.TestDTO{},
	)
	// инициализация базы данных sql и его адаптера
	dbx, sqlAdapter, err := db.NewSqlDB(a.conf.DB, tableScanner, a.logger)
	if err != nil {
		a.logger.Fatal("error init db", zap.Error(err))
	}
	// инициализация мигратора
	migrator := migrate.NewMigrator(dbx, a.conf.DB, tableScanner)
	err = migrator.Migrate()
	if err != nil {
		a.logger.Fatal("migrator err", zap.Error(err))
	}
	// инициализация кэша основанного на redis
	cacheClient, err := cache.NewCache(a.conf.Cache, decoder, a.logger)
	if err != nil {
		a.logger.Fatal("error init cache", zap.Error(err))
	}

	// инициализация хранилищ
	newStorages := storages.NewStorages(sqlAdapter, cacheClient)
	a.Storages = newStorages
	// инициализация сервисов
	services := modules.NewServices(newStorages, components)
	a.Servises = services

	// инициализация сервиса User в GRPC
	s := grpc.NewServer()
	userGRPCService := usr.NewServiceGRPC(services.User, tokenManager)
	usr.RegisterUserServiceServer(s, userGRPCService)

	// инициализация сервиса User в json RPC
	userRPC := user.NewUserServiceJSONRPC(services.User, tokenManager)
	jsonRPCServer := rpc.NewServer()
	err = jsonRPCServer.Register(userRPC)
	if err != nil {
		a.logger.Fatal("error init user json RPC", zap.Error(err))
	}

	if a.conf.RPCServer.GRPCorRPC == "GRPC" {
		//  инициализация сервера GRPC
		grpcServer := server.NewGRPC(a.conf.RPCServer, a.logger, s)
		go func() {
			err := grpcServer.Serve(context.Background())
			if err != nil {
				a.logger.Fatal("app: server error", zap.Error(err))
			}
		}()

	} else {
		//  инициализация сервера JRPC
		a.jsonRPC = server.NewJSONRPC(a.conf.RPCServer, jsonRPCServer, a.logger)
		go func() {
			err := a.jsonRPC.Serve(context.Background())
			if err != nil {
				a.logger.Fatal("app: server error", zap.Error(err))
			}
		}()
	}

	// инициализация клиента для взаимодействия с сервисом пользователей
	// client, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", a.conf.UserRPC.Host, a.conf.UserRPC.Port))
	// if err != nil {
	// 	a.logger.Fatal("error init rpc client", zap.Error(err))
	// }
	// a.logger.Info("rpc client connected")
	// userClientRPC := uservice.NewUserServiceJSONRPC(client)
	// a.Servises.Auth.SetUserer(userClientRPC)

	controllers := modules.NewControllers(services, components)
	// инициализация роутера
	r := router.NewRouter(controllers, components)
	// конфигурация сервера
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.conf.Server.Port),
		Handler: r,
	}
	// инициализация сервера
	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	// возвращаем приложение
	return a
}

// grpcAddress := "localhost:50051"
// listener, err := net.Listen("tcp", grpcAddress)
// if err != nil {
// 	log.Fatalf("failed to listen: %v", err)
// }
// if err := s.Serve(listener); err != nil {
// 	log.Fatalf("failed to serve: %v", err)
// }
