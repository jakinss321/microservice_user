package server

import (
	"context"
	"fmt"
	"net"

	"gitlab.com/jakinss321/microservice_user/config"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

type ServerGRPC struct {
	conf   config.RPCServer
	logger *zap.Logger
	srv    *grpc.Server
}

func NewGRPC(conf config.RPCServer, logger *zap.Logger, srv *grpc.Server) Server {
	// создание GRPC-сервера
	// srv := grpc.NewServer()

	// // регистрация реализации GRPC-сервера
	// userGRPCService := user.NewServiceGRPC(userService)
	// usrpb.RegisterUserServiceServer(srv, userGRPCService)

	return &ServerGRPC{conf: conf, logger: logger, srv: srv}
}

func (s *ServerGRPC) Serve(ctx context.Context) error {
	var err error

	chErr := make(chan error)
	go func() {
		l, err := net.Listen("tcp", fmt.Sprintf(":%s", s.conf.Port))
		if err != nil {
			s.logger.Error("grpc server register error", zap.Error(err))
			chErr <- err
		}

		s.logger.Info("grpc server started", zap.String("port", s.conf.Port))
		err = s.srv.Serve(l)
		if err != nil {
			s.logger.Error("grpc server error", zap.Error(err))
		}
	}()

	select {
	case <-chErr:
		return err
	case <-ctx.Done():
	}

	return err
}
