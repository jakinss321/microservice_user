package router

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/cors"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/component"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/middleware"
	"gitlab.com/jakinss321/microservice_user/pkg/modules"
	"gitlab.com/jakinss321/microservice_user/pkg/router"

	chimw "github.com/go-chi/chi/v5/middleware"
)

func NewRouter(controllers *modules.Controllers, components *component.Components) *chi.Mux {
	r := chi.NewRouter()
	setBasicMiddlewares(r)

	r.Mount("/", router.NewApiRouter(controllers, components))
	return r
}

func setBasicMiddlewares(r *chi.Mux) {
	proxy := middleware.NewReverseProxy()
	//corsHeader := middleware.NewCors()
	r.Use(chimw.Recoverer)
	r.Use(proxy.ReverseProxy)
	r.Use(
		cors.Handler(
			cors.Options{
				// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
				AllowedOrigins: []string{"https://*", "http://*"},
				// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
				AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
				AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
				ExposedHeaders:   []string{"Link"},
				AllowCredentials: false,
				MaxAge:           300, // Maximum value not ignored by any of major browsers
			},
		),
	)
	//r.Use(corsHeader.OpenAllCors)
	r.Use(chimw.RealIP)
	r.Use(chimw.RequestID)
}
