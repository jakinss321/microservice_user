package storages

import (
	"gitlab.com/jakinss321/microservice_user/pkg/db/adapter"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/cache"
	ustorage "gitlab.com/jakinss321/microservice_user/pkg/modules/user/storage"
)

type Storages struct {
	User ustorage.Userer
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User: ustorage.NewUserStorage(sqlAdapter, cache),
	}
}
