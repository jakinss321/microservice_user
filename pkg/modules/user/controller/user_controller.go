package controller

import (
	"net/http"

	"github.com/ptflp/godecoder"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/component"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/errors"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/handlers"
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/responder"
	"gitlab.com/jakinss321/microservice_user/pkg/modules/user/service"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	GetUsersInfo(http.ResponseWriter, *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

// @Summary Profile user
// @Security ApiKeyAuth
// @Description Получение профиля пользователя.
// @Tags user
// @Accept json
// @Produce json
// @Success 200 {object} ProfileResponse
// @Router /api/1/user/profile [get]
func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			User: *out.User,
		},
	})
}

func (u *User) GetUsersInfo(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}
