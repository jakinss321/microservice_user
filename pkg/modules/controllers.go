package modules

import (
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/component"
	ucontroller "gitlab.com/jakinss321/microservice_user/pkg/modules/user/controller"
)

type Controllers struct {
	User ucontroller.Userer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	userController := ucontroller.NewUser(services.User, components)

	return &Controllers{
		User: userController,
	}
}
