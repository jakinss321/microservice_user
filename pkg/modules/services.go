package modules

import (
	"gitlab.com/jakinss321/microservice_user/pkg/infrastructure/component"
	uservice "gitlab.com/jakinss321/microservice_user/pkg/modules/user/service"
	"gitlab.com/jakinss321/microservice_user/pkg/storages"
)

type Services struct {
	User          uservice.Userer
	UserClientRPC uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
	}
}
